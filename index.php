<?php

    ini_set('display_errors',1);

    function d($var, $die = false)
    {
        echo '<pre>'; print_r($var); echo '</pre>'; $die ? die() : null;
    }

    // підключаю клас
    include_once "SeoLink.php";
    // витягну рандомні метадані для даної сторінки
    $meta = SeoLink::getMeta('http://otakoyi.com/uk/rozrobka-saytiv');
    d($meta);

    echo SeoLink::displayLink('http://otakoyi.com/uk/rozrobka-saytiv');