# Призначений для генерації рандомних метатегів для посилання #

  Формат файлу з метаданими .csv

  Приклад даних в файлі

    url,name,title

    http://otakoyi.com/uk/rozrobka-saytiv,Розробка сайтів,Розробка сайтів Львів

    http://otakoyi.com/uk/rozrobka-saytiv,Розробка сайтів 1,Розробка сайтів Львів

    http://otakoyi.com/uk/rozrobka-saytiv,Розробка сайтів 2,Розробка сайтів Львів

   Delimiter можна поміняти в конфігу

  Буде створено файл кешу .cache, де будуть фіксуватись метатеги відносно $_SERVER['QUERY_STRING']

 
  Використання


```
#!php

<?php 

  // підключаю клас

    include_once "SeoLink.php";

    // отримаю масив метаданих для даної сторінки

    $meta = SeoLink::getMeta('http://otakoyi.com/uk/rozrobka-saytiv');

    /*вигляд масиву:
    Array
    (
       [name] => Розробка сайтів
       [title] => Розробка сайтів Львів
       [url] => http://otakoyi.com/uk/rozrobka-saytiv
    )*/

   // формую своє посилання

    echo "<a href='$meta['url']' title='$meta['title']'>{$meta['name']}</a>";


    // виводжу на екран посилання по шаблону для сторінки

    echo SeoLink::displayLink('http://otakoyi.com/uk/rozrobka-saytiv');


    // шаблон можна поміняти

    echo SeoLink::displayLink('http://otakoyi.com/uk/rozrobka-saytiv', "<a href='{url}' class='copy' title='{title}'>{name}</a>");
```